from ftplib import FTP
import os


def getClientReports(ftpIP,ftpDir,filesProcessedFilename):
    ftp=FTP(ftpIP)
    ftp.login(user='dtcc', passwd='M3dtcc')
    ftp.cwd(ftpDir)
    print("reading in previous files processed " + filesProcessedFilename)
    localDirectory = 'ftp/clientFiles/'
    if not os.path.exists(localDirectory):
        os.makedirs(localDirectory)
    try:
        with open(localDirectory + filesProcessedFilename, 'w') as fp:
            ftp.retrlines('RETR ' + filesProcessedFilename, lambda s, w=fp.write: w(s + '\n'))  # saving line by line
        with open(localDirectory + filesProcessedFilename, 'r') as fp:
            filesProcessed = [filename[:-1] for filename in fp]  # removing n char from the end
        filesProcessed.append(filesProcessedFilename)
    except:
        print("did not find previous files processed list "+filesProcessedFilename)
        filesProcessed = []

    #find any reports that haven't processed already
    filesToProcess=[]
    for file in ftp.nlst():
        if not file in filesProcessed:
            destFileName = localDirectory + file
            with open(destFileName,'w') as destFile:
                ftp.retrlines('RETR ' + file, lambda s, w=destFile.write: w(s + '\n'))
            filesToProcess.append(destFileName)
    print('found '+str(len(filesToProcess))+' new files to process')
    return filesToProcess
