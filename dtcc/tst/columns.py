import unittest
from dtcc.columns.naCollateralFields import naCollateralFields
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames
from dtcc.columns.naValuationFields import naValuationFields

class test_columns(unittest.TestCase):
	def testColumns(self):
		self.assertTrue(set(naCollateralFields).issubset(DTCC_ColumnNames))
		self.assertTrue(set(naValuationFields).issubset(DTCC_ColumnNames))

if __name__=="__main__":
	unittest.main()