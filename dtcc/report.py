# -*- coding: utf-8 -*-
#reports
##rejection reports should probably have headers and be ugly in terms of structure.
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames
import dtcc

#########
#Reports
#1 ACK/NACK Report(Upload Status Report)
	#The ACK or NACK output report will be a real-time report indicating status of each message submitted. ACK report will include all accepted messages and NACK report will include all rejected messages.
#2 Warning Report
	#The warning report highlights warnings of issues that may indicate the message is not compliant for regulator reporting even if the GTR accepted the message.
#dont do??? 3 GTR ESMA OTC Position Report
	#Includes the latest Position for all open UTI�s that are reported to ESMA. The report will contain only the ESMA reportable fields from Counterparty Data (Table 1) and Common Data (Table 2). All alleges will be included regardless of what service the UTI was reported through.
#dont do??? 4 GTR ESMA OTC Activity Report
	#Includes all successfully processed submissions into the GTR since the last report generation for all UTI�s that are reported to ESMA. The report will contain only the ESMA reportable fields from Counterparty Data (Table 1) and Common Data (Table 2). All alleges will be included regardless of what service the UTI was reported through.
#5 ESMA Match Status Report
	#Reports of the pairing and match status by UTI. Any unmatched fields are listed.
	##pairing report
	###Trade Pairing Status - Trade Pairing Status Description
	###-------------------------------------------------------
	###Exempt - The position will not require pairing as the Counterparty Region is non-EEA.
	###N/A - The positions will not require pairing as the Reporting Obligation does not include ESMA.
	###Unpaired - Pairing has either yet to be attempted or that no pair has been located thus far.
	###Unpairable - The position does meet the ESMA reconciliation criteria but does not have mandatory fields required for the Pairing process (such as UTI and LEI�s).
	###Paired - The Position has been successfully paired either within the GTR or with another TR.
	###Pairing Error - When a position has had external Pairing attempted but more than one TR has responded with the same positions details.
	###Pair LEI Break - When a position pair has been found where the UTI and one LEI match but the other LEI does not match.
#???? 6 UTI Conflict and Pair Break LEI Report
	#List of all trades where the LEI pair submitted is different for each trade submission for the same UTI.

#custom
def fileProcessingReport(workDir,fileValidity):
	pass

#file=open(workDir+'/fileProcessingReport.csv','a')
#file.write('filename,status,error,time\n')
#for fileStatus in fileValidity:
#	file.write(','.join(fileStatus.append(now()))+'\n')
#file.flush()

def ackNackReport(ack,nack,fileName):
	#DECISION: whole report with a column added 'status' which indicates 'accepted'/'rejected'
	with open(dtcc.workDir+fileName.split('.csv')[0]+'_ack.csv','w')as f:
		f.write(','.join(DTCC_ColumnNames)+'\n')
		for line in ack:
			f.write(line+'\n')
		f.flush()
	with open(dtcc.workDir+fileName.split('.csv')[0]+'_nack.csv','w')as f:
		f.write(','.join(DTCC_ColumnNames)+'\n')
		for line in nack:
			f.write(line+'\n')
		f.flush()
def warningReport(warn,fileName):
	#DECISION: details of why a message was rejected in the NACK report, AND minor warnings about messages accepted in the ACK report. Format list of :
		#UTI, msgtype, msgrow num
			#field:warning
			#..
		#..
	with open(dtcc.workDir+fileName.split('.csv')[0]+'_ack.csv','w')as f:
		f.write('fileName,msgId,currentCol,warning\n')
		for line in warn:
			f.write(line+'\n')
		f.flush()
	
#def GTR_ESMA_OTC_Position_Report:
	
#def GT_ESMA_OTC_Activity_Report:
	
def ESMA_Match_Status_Report():
	pass
	#Pairing report
	#DECISION: format:
		#file, UTI, msgtype, msgrow num
			#Exempt, N/A, Unpaired, Unpairable, Paired, Pairing Error, Pair LEI Break
			#Unmatched fields:
				#field:value 1, value 2
				#..
		#..
###def UTI_Conflict_and_Pair_Break_LEI_Report:
