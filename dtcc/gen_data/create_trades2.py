#!/opt/rh/rh-python35/root/usr/bin/python3
#This is a test file



import sqlalchemy

def connect(db_path): 
	global engine
	engine = sqlalchemy.create_engine(db_path)
	global con
	con = engine.connect()
	return engine,con

def make_table(table_name, db_path):
    engine, con = connect(db_path)
    try:
        con.execute(f'''CREATE table {table_name}(date_time TIMESTAMP, 
                                                  instrument varchar(20),
                                                  price float, 
                                                  quantity numeric(10,0),
                                                  side varchar(1),
                                                  exch varchar(6),
                                                  trade_ID int NOT NULL AUTO_INCREMENT,
                                                  CP varchar(20),
                                                  client varchar(20),
                                                  PRIMARY KEY(trade_ID))''')
        print(f"Table {table_name} created successfully. ")
    except Exception as e:
        print(f"Error creating table {table_name}.    " + str(e))


