# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 13:47:46 2019

@author: wiecz
"""

from create_trades2 import connect
import sqlalchemy
import pandas as pd
    
def migrateDB(pathDB1, pathDB2):
    try:
        
        engine1, con1 = connect(pathDB1)
        engine2, con2 = connect(pathDB2)
        
        tableNames = engine1.table_names()
        metadata = sqlalchemy.MetaData(bind=engine1)
        
        for name in tableNames:
            table = sqlalchemy.Table(name, metadata, autoload=True, autoload_with=engine1)
            table.create(engine2)
            data = pd.read_sql_table(name, engine1)
            data.to_sql(name, engine2, index = False, if_exists = 'append')
            print('Table: '+ str(name) + ' Successful migrated')
    except:
        print('Error during migration')