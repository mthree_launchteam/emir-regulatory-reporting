# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 12:56:47 2019

@author: wiecz
"""

from create_trades2 import make_table
from insert_trades import insertTrades, generateTradeData

def generateDB(x,path, table_name):
    make_table(table_name, path)
    i=0
    while i< x:
        insertTrades(path, table_name, generateTradeData())
        i += 1

tableName = 'emir'
DBpath = "mysql+pymysql://root:@localhost:3306/project"