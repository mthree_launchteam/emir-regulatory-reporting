#!/opt/rh/rh-python35/root/usr/bin/python3
import random
import string
from create_trades2 import connect


def generateTradeData():
    # datetime
    date_time = '2019.07.' + str(random.randint(1, 14)) + ' ' + str(random.randint(0, 23)) + ':' + str(random.randint(0,59)) + ':' + str(random.randint(0,59))
    
    # instrument
    instrument = []
    for x in range(3):
        instrument.append(random.choice(string.ascii_letters).upper())
        
    for x in range(15):
        instrument.append(str(random.randint(0,9)))
            
    instrument = ''.join(instrument)

    # price
    price = random.uniform(1, 100)
    
    # quantity
    quantity = random.randint(1, 10000)
                          
    # side 
    side = random.choice('SB')
                          
    # exch
    exch = []
    for x in range(4):
        exch.append(random.choice(string.ascii_letters).upper())
    exch = ''.join(exch)
    
    # CP
    lett_digit = string.ascii_letters + string.digits
    CP = ''.join(random.choice(lett_digit) for i in range(18)).upper()
    
    # client
    client = 'Name' + str(random.randint(1, 100))
    
    return {'date_time':date_time,
           'instrument':instrument,
           'price':price,
           'quantity':quantity,
           'side':side,
           'exch':exch,
           'CP':CP,
           'client':client}

def insertTrades(db_path, table_name, dict_values):
    engine, con = connect(db_path)
    columns = str(list(dict_values.keys())).replace("'", "").replace('[', '').replace(']', '')
    values = str(list(dict_values.values())).replace('[', '').replace(']', '')
    print(f'''insert into {table_name}({columns})
                values ({values})''')
    con.execute(f'''insert into {table_name}({columns})
                values ({values})''')
    

