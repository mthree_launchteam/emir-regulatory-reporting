#!/opt/rh/rh-python35/root/usr/bin/python3 -u
# -*- coding: utf-8 -*-
from ftplib import FTP
import datetime
from dtcc.validation.checkFileStructure import checkFileStructure
from dtcc.validation.parseLine import parseLine
from dtcc.validation.checkFooter import checkFooter
from dtcc.validation.checkESMA import checkESMA
from dtcc.validation.pairing import pairing
from dtcc.validation.util import readline, err
from dtcc.ftp.getClientReports import getClientReports
from dtcc.report import ackNackReport, warningReport, fileProcessingReport,	ESMA_Match_Status_Report
import dtcc

fileValidity=[]
filesToProcess=getClientReports(dtcc.ftpIP,dtcc.ftpDir,dtcc.filesProcessedFilename)
#filesToProcess=getClientReports('192.168.8.190','/teamB/','filesProcessed.txt')
for fileName in filesToProcess:
    try:
        with open(fileName,'rb')as currentFile:
            print ("\n------fileName: "+fileName+"------")
            with open(fileName,'r') as f:
                print(f.read())
            l,numExpectedCols=checkFileStructure(currentFile) #l=current line number
            beforeBody=l
            dtcc.msgIds=[] #reset
            dtcc.ack=[] #stores the accepted lines+',accept'
            dtcc.nack=[] #reset
            dtcc.warn=[] #reset
            bodyLength=0
            firstFooterLine='Constant1,DTC0-END00054321'
            l,line=readline(l, currentFile)
            while line!=firstFooterLine:
                if line=='':
                    err('There should not be a blank line here.',l)
                parseLine(l,line,numExpectedCols)
                #l+=1  incrementing l twice (!!)
                bodyLength = l - beforeBody
                l, line = readline(l,currentFile)
            if bodyLength==0:
                err('no body')
            checkFooter(bodyLength, currentFile, l-1)
            #TODO transactions
        if not len(dtcc.nack):
            continue
        for ackLine in dtcc.ack:
            checkESMA(ackLine) #Structure of file and format of fields is correct, now check the business logic#raises ValueError if any rule is broken
        #pairing(dtcc.workDir,fileName)
    except Exception as e:
        print(e)
        fileValidity.append([fileName,False,str(e)])
        continue
    fileValidity.append([fileName,True,None])
    ackNackReport(dtcc.ack,dtcc.nack,fileName)
    warningReport(dtcc.warn,fileName)
print("------ALL FILES PROCESSED------")
fileProcessingReport(dtcc.workDir,fileValidity)
#TODO upload fileProcessingReport.csv
#TODO implement delays between ackNack+warning reports and the ESMA_Match report, during which time the other side reports.
ESMA_Match_Status_Report()

ftp=FTP(dtcc.ftpIP)
ftp.login(user='dtcc', passwd='M3dtcc')
ftp.cwd(dtcc.ftpDir)
now=str(datetime.datetime.now())
report_filename='report'+now+'.csv'
#ftp.storbinary('STOR '+report_filename,f)


#add to already processed list
#...
#append to already processed list file
#...

#f.close
ftp.quit()


# NOTES#################################
# Warnings will be generated for all blank ESMA required fields. While the GTR might have a field as optional the field maybe an ESMA required field. It is the party’s responsibility to ensure that they are fully compliant for reporting to ESMA.

# UTI
##up to 52 chars
##i remember the UTI not being long enough to fit the time in there.
##The uniqueness of a UTI will be determined based on the combined values of the UTI Prefix and
##the UTI Value

# DESIGN##################
# check for updated or new files on the ftp server
# any file starting with valuation*
# might be more than one due to multiple teams or multiple runs

# read the files from the ftp site(don't need to download?)

# for each file
# parse it line by line into a list of dictionaries
# construct a string of any issues

# write out issues file, probably even if no issues, would just be empty
# generate fake counterparty report by fiddling with the data
# lookup in oracle
# do a better calculation than the sample code by including costs
# follow below algorithm to try to ensure differences
# fiddle with the id numbers and ccp etc so it truly is as if someone else did it. for each ccp have a different report time to make it look like an aggregation of ccps
# for each ccp
# first one compress the buys
# second one compress the sells
# third one compress both sides
# fourth ommit completely
# fifth fiddle with times so they don't quite match
# sixth fiddle with row ordering within the transactions of this uti
# seventh put this uti first so that the whole thing is out of order
# 8,9,10th put all transactions, then positions, then valuations together
# for a non-eea swap or something, change the valuation time and fiddle with the mtm due to taking fx rate at a different time

# persist counterparty report

# read in each file
# validate it cell by cell, and generate rejection report if find issues #can test this by validating our fake ccp report
# .....