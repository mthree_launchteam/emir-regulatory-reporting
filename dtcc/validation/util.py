import re
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames
from dtcc import cols
import dtcc

###############################
#Error handling
###############################
#dtcc_main.py reads in emir reports and validates them, it has to look at each record/field, if the data is invalid then this function generates the error
#it relies on a currentCol global variable which keeps track of which column we are processing
#fundamental format errors causes processing of the file to stop
#TODO consider making currentCol an argument 
currentCol=None
def err(text,l=0,index=None): #TODO what is index for?
	colNum='' if currentCol is None else '. Column '+currentCol+' . '
	raise ValueError('error: line:'+str(l)+' col:'+colNum+'. '+text)
#if the file is in the correct format, but is logically inconsistent, then we compile a list of rejection reasons
def rej(text):
	#TODO we are putting duplicates in the nack, is that intentional? surely not?
	dtcc.nack.append(text) #append this line to list of rejected lines
	dtcc.warn.append("msgId="+dtcc.msgIds[-1]+" "+text)
###############################
#File processing
###############################
#Python3 makes reading a line ugly, so we wrap it here to make the code more readable
def readline(l, currentFile):
	#global l #l is the current line number
	l = l + 1
	line = currentFile.readline().decode("utf-8")[:-1]
	line = line.strip()
	return l,line
	#return line, dtcc.currentFile.readline().decode("utf-8")[:-1] #we don't want the \n at the end
####################
#Given the name of the column, find it's index and return the value from the current record 
def get(name):
	return cols(DTCC_ColumnNames.index(name))


###############################
#Field validation
###############################
#validate datetime format of the current field
#TODO rename to v_datetime
#False is good, True is bad
def v_datetime(index):
	if not isinstance(index,int):
		return True
	else:
		if dtcc.cols(index)is'': #if value we are validating is blank, that is a valid date
			return False
		return not re.match(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$',dtcc.cols(index))

#validate float number format, maybe allow nulls
def number(index,canBeNull=True):
    if not isinstance(index, int):
        return True
    if canBeNull and (dtcc.cols(index) is '' or dtcc.cols(index) == 0): #null date can be missing or 0
        return False
    if not re.match(r'.*[a-zA-Z].*^$' ,dtcc.cols(index)):
        print('numbers must not contain letters')
        return True
    whole,fractional=dtcc.cols(index).split('.')
    if len(whole)>25:
        print('numbers must be at most 25 whole digits and 10 fractional')
        return True
    if len(fractional)>10:
        print('numbers must be at most 25 whole digits and 10 fractional')
        return True
    try: #the casts will error if the string isn't a number so catch and reject
        if whole[0]=='-':
            int(whole[1:])
        else:
            int(whole)
        int(fractional)
        return False
    except:
        print('not a number')
        return True
#validate int number format, maybe allow nulls
def wholeNumber(index):
	try:
		int(dtcc.cols(index))
		return False
	except:
		return True
#TODO rename to v_date
#validate date
def v_date(index):
    try:
        if dtcc.cols(index)is'':
            return False
        return not re.match(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}',dtcc.cols(index))
    except:
        return True

###############################
#Formatting
###############################
#format list as quoted csv
#TODO is there an inbuilt for this in python3?
def quote(list_):
	return  "'"+str("','".join(list_))+"'"

#############
#TODO replace with pandas	
class table:
	def __init__(self,square=[]):
		self.data=[self.__parse(line) for line in square]
	def __parse(self, line):
		return {key:value for key,value in zip(DTCC_ColumnNames,line.split(','))}

	def c(self,colName): #column
		col=[row[colName]for row in self.data]
		return col
	def s(self,col,op,value): #select
		[row for row in self.data if eval("'"+row[col]+"'"+op+"'"+value+"'")]
	def i(self,newData): #insert
		duplicateRows=[]
		#TODO make this more sophisticated by processing cancels/modifies
		for line in newData:
			newRow=self.__parse(line)
			if newRow in self.data:
				duplicateRows+=int(newRow['Data Submitter Message ID'])
			else:
				self.data.append(newRow)
		return duplicateRows

