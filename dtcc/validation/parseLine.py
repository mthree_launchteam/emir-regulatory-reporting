from dtcc.validation.util import rej
import dtcc
from dtcc.validation.parseRules import rule
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames

def parseLine(l,line, numExpectedCols):
	nacksBefore=len(dtcc.nack)
	if line=='':
		rej('unexpected end of file, expected to find footer starting with\nConstant1,DTC0-END00054321')
	dtcc.cols=line.split(',')
	if len(dtcc.cols)!=numExpectedCols:
		rej('Expected '+str(numExpectedCols)+' columns, got:'+str(len(dtcc.cols)))
	if dtcc.cols[8] in dtcc.msgIds:
		rej('Data Submitter Message ID must be unique within the file')
	dtcc.msgIds.append(dtcc.cols[8])

	lineOK = True
	for i in range(len(DTCC_ColumnNames)):
		if rule(i): #test all the data format rules and record which lines get rejected
			lineOK = False
			break
	if lineOK:
		dtcc.ack.append(str(l)+line + ',accept')
	else:
		rej(str(l)+line +',reject')
	return
	##Event ID Party 1
	#cols(10)==''
	##Event ID Party 2
	#cols(11)==''

	#Trade Party 1 Value
	#cols(17)
	
	#Trade Party 2 Value
	#cols(19)
	
	##Broker Id Party 1 Value
	#cols(31)
	
	##Broker Id Party 2 Value
	#cols(33)
	
	##Data Submitter Value
	#cols(35)
	
	##Submitted For Value
	#cols(37)
	
	##Clearing Broker Party 1 Value
	#cols(40)

	##Clearing Broker Party 2 Value
	#cols(42)
	
	##Execution Agent Party 1 Value
	#cols(44)
	
	##Execution Agent Party 2 Value
	#cols(46)
	
	##MTM Currency Party 1
	#cols(64)
	##MTM Currency Party 2
	#cols(65)
	##MTM Currency CCP
	#cols(66)

	##Currency of the collateral value Party 1 (repeatable values)
	#cols(79)
	##Currency of the collateral value Party 2 (repeatable values)
	#cols(80)

	##Product ID Value 1
	#cols(82)
	
	##Product ID Value 2
	#cols(84)
	
	##Settlement Currency (repeatable up to 2 times semi-colon separated)
	#cols(87)
	##Prior UTI Prefix (repeatable)
	#cols(88)
	
	##Transaction Reference Number
	#cols(90)
	##Internal Trade reference
	#cols(91)

	##Upfront payment CCY (repeatable up to 6 times semi-colon separated)
	#cols(100)

	##Clearing DCO Value
	#cols(110)
	
	##Option Strike Price CCY
	#cols(115)

	##Fixed leg payment frequency (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	#cols(121)
	##Floating rate payment frequency (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	#cols(122)
	##Floating rate reset frequency (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	#cols(123)
	##Floating rate of leg 1 (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	#cols(124)
	##Floating rate of leg 2 (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	#cols(125)
	##Currency 2 (Foreign exchange only)
	#cols(126)
	
	##Exchange rate basis (Foreign exchange only)
	#cols(129)
	
	##Delivery point or zone (Commodities only - energy) (repeatable up to 2 times semi-colon separated)
	#cols(132)
	##Interconnection Point (Commodities only - energy)
	#cols(133)
	
	##Contract capacity (Commodities only - energy) (repeatable up to 24 times semi-colon separated)
	#cols(137)
	
	##Placeholder
	#cols(146)
	
	#TODO add support for these repeatable fields 148 etc

	##Option Strike Price Type
	#cols(154)
	##Trade Link ID
	#cols(155)