import dtcc
from dtcc.validation.util import rej, quote, v_datetime, wholeNumber, v_date,\
	number
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames

def rule(i):
	val=dtcc.cols[i]
	return rules[i][0](val)

		#print(DTCC_ColumnNames[i]+":"+rules[i][1](val))
		#rej(DTCC_ColumnNames[i]+":"+rules[i][1](val))


#bunch of variables used in checks and error messages
messageTypes=['Position','Transaction','Valuation','Collateral']
actions=['New','Cancel']
transactions=['Trade',  'Exit', 'Backload', 'PositionCancel']
assets=["Credit", "InterestRate", "ForeignExchange", "Equity", "Commodity"]
companyIdTypesOrBlank=[""]+dtcc.companyIdTypes
companyIdTypesOrBlankString=','.join(dtcc.companyIdTypes)
esma=['','ESMA']
corporateSectors=['',"A","C","F","I","L","O","R","U"]
companyIdTypesExFF=["LEI","CICI","DTCC","AVOX","SWIFTBIC","EIC"]
delegationModels=['',"Independent","Full"]
delegationModelsString=''.join(delegationModels)
capacityTypes=["Principal","Agent"]
eea=["EEA","nonEEA"]
bool_=['',"true", "false"]
mark=['',"MarkToMarket","MarkToModel"]
collateral=['',"Uncollateralized","Partially","OneWay","Fully"]
productTypes=['','U','I','E','ISDA']
venuePrefix=["MIC","LEI","CICI","DTCC","AVOX","FREEFORMATTEXT"]
priceTypes=["", "Basis Points", "Percentage", "Currency Amount", "Price", "Spread", "Rate"]
deliveryTypes=['',"C","P","O"]
optionTypes=['','P','C']
optionStyles=['',"European","American","Bermudan","Asian "]
lifecycles=['',"Trade","New","Termination","PartialTermination","Compression","ValuationUpdate","Increase","Exercise","PartialExercise","Novation","Novation-Trade","NovationTrade","CorporateAction","Amendment","Modify","Cancel","Exit","Error","Backload","CreditEvent"]
dayCounts=['',"Actual/365","30B/360","Other"]
commoditiesBase=['',"AG","EN","FR","ME","IN","EV","EX"]
commodityDetails=['',"GO","DA","LI","FO","SO","OI","NG","CO","EL","IE","PR","NP","WE","EM"]
energyCodes=['',"BL","PL","OP","BH","OT"]
confirmationTypes=["", "NonElectronic","NotConfirmed","Electronic"]
assetIdTypes=['',"LEI","CICI","FREEFORMATTEXT","ISIN","UPI","Basket","Index"]
tradeParties=["","Trade Party 1","Trade Party 2"]
datetimeErr='must be a datetime in format "YYYY-MM-DDTHH:MM:SSZ"'
dateError='must be a date in format "YYYY-MM-DD"'
numberError='not a number'

#TODO some of the .joins are wrong as "" won't show up. and the start and end ' are missing, so change to use quote function

#start on line 50 so easy to see numbers#list of tuples, each tuple is (lambda rule to check input x, lambda to generate error message if needed) next line is line 50 which is rule 0(index 0), line 51 is rule 1(index 1) etc
rules=(
	(lambda x:False,                         lambda x:''), #rule 0 #0 is cOmment so don't care about the value####Body#field formats######28 control fields###############
	(lambda x:x!='Lite 1.0',                  lambda x:'expected Version to be "Lite1.0"'),#Version
	(lambda x:x not in messageTypes,         lambda x:'expected Message Type to be one of:'+','.join(messageTypes)),#Transaction Type
	(lambda x:x not in actions,              lambda x:'expected Action to be one of:'+','.join(actions)),
	(lambda x:x not in transactions,         lambda x:'expected "Transaction Type" to be one of:'+','.join(transactions)),#UTI Prefix -max length 40, Optional value
	(lambda x:len(x)>40 and not '',          lambda x:'UTI prefix max length is 40, '+str(len(x))+' is too long.'),#UTI Value -max length 200, required#TODO 6+7 max 52
	(lambda x:len(x)>200 and len(x)>0,       lambda x:'UTI value max length is 200, '+str(len(x))+' is to long.'),
	(lambda x:x.lower() not in assets,       lambda x:'expected Primary Asset Class to be one of:'+','.join(assets)),
	(lambda x:not 0<len(x)<=256,             lambda x:'Data Submitter Message ID is too long, max is 256 chars'),
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),#As of Date/Time
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'expected Additional Repository Prefix to be in '+companyIdTypesOrBlankString),#Additional Repository 1 Prefix
	(lambda x:len(x)>50,                     lambda x:'Additional Repository 1 Value must be <=50 chars'),#####128 EMIR fields################Additional Repository 1 Value
	(lambda x:x!='ESMA',                     lambda x:'Reporting Obligation Party 1 must be ESMA'),#Reporting Obligation Party 1
	(lambda x:x not in esma,                 lambda x:'Reporting Obligation Party 1 must be ESMA or ""'),#Reporting Obligation Party 2
	(lambda x:x not in dtcc.companyIdTypes,  lambda x:'Trade Party 1 Prefix must be one of '+dtcc.companyIdTypesString),#Trade Party 1 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in dtcc.companyIdTypes,  lambda x:'Trade Party 2 Prefix must be one of '+dtcc.companyIdTypesString),#Trade Party 2 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:len(x)>500,                    lambda x:'Trade Party 1 Domicile cannot be longer than 500 characters'),#Trade Party 1 Domicile
	(lambda x:len(x)>500,                    lambda x:'Trade Party 2 Domicile cannot be longer than 500 characters'),#Trade Party 2 Domicile
	(lambda x:len(x)>2,                      lambda x:'Trade Party 1 Branch Location cannot be longer than 2 characters'),#Trade Party 1 Branch Location
	(lambda x:len(x)>2,                      lambda x:'Trade Party 2 Branch Location cannot be longer than 2 characters'),#Trade Party 2 Branch Location
	(lambda x:x not in corporateSectors,     lambda x:'Trade Party 1 Corporate Sector must be one of:'+','.join(corporateSectors)),#Trade Party 1 Corporate Sector
	(lambda x:x not in corporateSectors,     lambda x:'Trade Party 2 Corporate Sector must be one of:'+','.join(corporateSectors)),#Trade Party 2 Corporate Sector
	(lambda x:x not in esma,                 lambda x:'Trade Party 1 Financial Entity Jurisdiction should be one of '+quote(esma)),#Trade Party 1 Financial Entity Jurisdiction
	(lambda x:x not in esma,                 lambda x:'Trade Party 1 Non-Financial Entity Jurisdiction should be be one of '+quote(esma)),#Trade Party 1 Non-financial Entity Jurisdication
	(lambda x:x not in esma,                 lambda x:'Trade Party 2 Financial Entity Jurisdiction should be one of '+quote(esma)),#Trade Party 2 Financial Entity Jurisdiction
	(lambda x:x not in esma,                 lambda x:'Trade Party 2 Non-financial Entity Jurisdiction should be one of '+quote(esma)),#Trade Party 2 Non-financial Entity Jurisdiction
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Broker Id Party 1 Prefix should be one of '+companyIdTypesOrBlankString),#Broker Id Party 1 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Broker Id Party 2 Prefix should be one of '+companyIdTypesOrBlankString),#Broker Id Party 2 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesExFF,   lambda x:'Data Submitter Prefix should be in '+quote(companyIdTypesExFF)),#Data Submitter Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Submitted For Prefix should be one of '+companyIdTypesOrBlankString),#Submitted For Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in delegationModels,     lambda x:'Reporting Delegation Model should be in '+delegationModelsString),#Clearing Broker Party 1 Prefix
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Clearing Broker Party 1 Prefix should be one of '+companyIdTypesOrBlankString),#Reporting Delegation Model
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Clearing Broker Party 2 Prefix should be one of '+companyIdTypesOrBlankString),#Clearing Broker Party 2 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Execution Agent Party 1 Prefix should be one of '+companyIdTypesOrBlankString),#Execution Agent Party 1 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Execution Agent Party 2 Prefix should be one of '+companyIdTypesOrBlankString),#Execution Agent Party 2 Prefix
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Beneficiary ID Party 1 Prefix should be one of '+companyIdTypesOrBlankString),                      #Beneficiary ID Party 1 Prefix                                       
	(lambda x:len(x)>50,                     lambda x:"Beneficiary ID Party 1 Value can't be longer than 50 characters"),                                  #Beneficiary ID Party 1 Value                                        
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Beneficiary ID Party 2 Prefix should be one of '+companyIdTypesOrBlankString),                      #Beneficiary ID Party 2 Prefix                                       
	(lambda x:len(x)>50,                     lambda x:'Beneficiary ID Party 2 Value cant be longer than 50 chars'),                                        #Beneficiary ID Party 2 Value                                        
	(lambda x:x not in capacityTypes,        lambda x:'Trading capacity Party 1 must be one of '+quote(capacityTypes)),                                    #Trading capacity Party 1                                            
	(lambda x:x not in['']+capacityTypes,    lambda x:'Trading capacity Party 2 must be in '+quote(['']+capacityTypes)),                                   #Trading capacity Party 2                                            
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Buyer Prefix (Party 1) should be one of '+companyIdTypesOrBlankString),                             #Buyer Prefix (Party 1)                                              
	(lambda x:len(x)>50,                     lambda x:'Buyer Value (Party 1) cant be longer than 50 chars'),                                               #Buyer Value (Party 1)                                               
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Buyer Prefix (Party 2) must be in '+companyIdTypesOrBlankString),                                   #Buyer Prefix (Party 2)                                              
	(lambda x:len(x)>50,                     lambda x:'Buyer Value (Party 2) cant be longer than 50 chars'),                                               #Buyer Value (Party 2)                                               
	(lambda x:x not in eea,                  lambda x:'Party Region must be in '+quote(eea)),                                                              #Party Region                                                        
	(lambda x:x not in eea,                  lambda x:'Counterparty Region must be in '+quote(eea)),                                                       #Counterparty Region                                                 
	(lambda x:x not in bool_,                lambda x:'Directly linked to commercial activity or treasury financing Party 1 must be in '+quote(bool_)),    #Directly linked to commercial activity or treasury financing Party 1
	(lambda x:x not in bool_,                lambda x:'Directly linked to commercial activity or treasury financing Party 2 must be in '+quote(bool_)),    #Directly linked to commercial activity or treasury financing Party 2
	(lambda x:number(x),                     lambda x:numberError),                                                                                                 #MTM Value Party 1                                                   
	(lambda x:number(x),                     lambda x:numberError),                                                                                                 #MTM Value Party 2                                                   
	(lambda x:number(x),                     lambda x:numberError),                                                                                                 #MTM Value CCP                                                       
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                 #Valuation Datetime Party 1                         
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                 #Valuation Datetime Party 2                         
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                 #Valuation Datetime CCP                             
	(lambda x:x not in mark,                 lambda x:'Valuation Type Party 1 must be in '+quote(mark)),                    #Valuation Type Party 1                             
	(lambda x:x not in mark,                 lambda x:'Valuation Type Party 2 must be in '+quote(mark)),                    #Valuation Type Party 2                             
	(lambda x:x not in mark,                 lambda x:'Valuation Type CCP must be in '+quote(mark)),                        #Valuation Type CCP                                 
	(lambda x:x not in collateral,           lambda x:'Collateralized Party 1 must be in '+quote(collateral)),              #Collateralized Party 1                             
	(lambda x:x not in collateral,           lambda x:'Collateralized Party 2 must be in '+quote(collateral)),              #Collateralized Party 2                             
	(lambda x:len(x)>50,                     lambda x:'Collateral portfolio code Party 1 cant be longer than 50 chars'),    #Collateral portfolio code Party 1                  
	(lambda x:len(x)>50,                     lambda x:'Collateral portfolio code Party 2 cant be longer than 50 chars'),    #Collateral portfolio code Party 2                  
	(lambda x:number(x),                     lambda x:numberError),                                                                  #Value of the collateral Party 1 (repeatable values)
	(lambda x:number(x),                     lambda x:numberError),                                                                  #Value of the collateral Party 2 (repeatable values)
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in productTypes,         lambda x:'Product ID Prefix 1 must be in '+quote(productTypes)),               #Product ID Prefix 1
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in productTypes,         lambda x:'Product ID Prefix 2 must be in '+quote(productTypes)),               #Product ID Prefix 2
	(lambda x:False,                         lambda x:''),
	(lambda x:len(x)>200,                    lambda x:'Underlying Asset (repeatable up to 2 times semi-colon separated) cant be longer than 200 chars'), #Underlying Asset (repeatable up to 2 times semi-colon separated)
	(lambda x:len(x)>50,                     lambda x:'Notional Currency/Units (repeatable up to 2 times semi-colon separated) cnat be longer than 50 chars'), #Notional Currency/Units (repeatable up to 2 times semi-colon separated)
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:len(x)>52,                     lambda x:'Prior UTI Value (repeatable) cant be longer than 52 chars'), #Prior UTI Value (repeatable)
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in venuePrefix,          lambda x:'Execution Venue Prefix '+quote(venuePrefix)),                  #Execution Venue Prefix                                         
	(lambda x:len(x)!=4,                     lambda x:'Execution Venue Value must be 4 characters'),                  #Execution Venue Value                                          
	(lambda x:number(x),                     lambda x:numberError),                                                            #Price Notation - Price                                         
	(lambda x:x not in priceTypes,           lambda x:'Price Notation - Price Type must be in '+quote(priceTypes)),   #Price Notation - Price Type                                    
	(lambda x:wholeNumber(x),                lambda x:'must be a whole number'),                                                            #Notional Amount (repeatable up to 2 times semi-colon separated)
	(lambda x:number(x),                     lambda x:numberError),                                                            #Price Multiplier                                               
	(lambda x:number(x),                     lambda x:numberError),                                                            #Quantity                                                       
	(lambda x:number(x),                     lambda x:numberError),                                                            #Upfront payment (repeatable up to 6 times semi-colon separated)
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in deliveryTypes,        lambda x:'Delivery type must be in '+quote(deliveryTypes)),              #Delivery type                                                     
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                           #Execution Timestamp                                               
	(lambda x:v_date(x),                     lambda x:dateError),                                                            #Effective Date (repeatable up to 2 times semi-colon separated)    
	(lambda x:v_date(x),                     lambda x:dateError),                                                            #Scheduled Termination Date                                        
	(lambda x:v_date(x),                     lambda x:dateError),                                                            #Termination Date                                                  
	(lambda x:v_date(x),                     lambda x:dateError),                                                            #Date of Settlement (repeatable up to 2 times semi-colon separated)
	(lambda x:x not in bool_,                lambda x:'Cleared must be in '+quote(bool_)),                            #Cleared                                                           
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                           #Clearing Timestamp                                                
	(lambda x:x not in companyIdTypesOrBlank,lambda x:'Clearing DCO Prefix must be in '+companyIdTypesOrBlankString), #Clearing DCO Prefix                                               
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in bool_,                lambda x:'Intragroup must be in '+quote(bool_)),                         #Intragroup         
	(lambda x:x not in optionTypes,          lambda x:'Option Type must be in '+quote(optionTypes)),                  #Option Type        
	(lambda x:x not in optionStyles,         lambda x:'Option Style '+quote(optionStyles)),                           #Option Style       
	(lambda x:number(x),                     lambda x:numberError),                                                            #Option Strike Price
	(lambda x:False,                         lambda x:''),
	(lambda x:v_date(x),                     lambda x:dateError),                                                                                                                         #Lifecycle event Effective Date                                                            
	(lambda x:x not in lifecycles,           lambda x:'Lifecycle Event must be in '+quote(lifecycles)),                                                                            #Lifecycle Event                                                                           
	(lambda x:number(x),                     lambda x:numberError),                                                                                                                         #Fixed rate of leg 1 (Interest Rates only)                                                 
	(lambda x:number(x),                     lambda x:numberError),                                                                                                                         #Fixed rate of leg 2 (Interest Rates only)                                                 
	(lambda x:x not in dayCounts,            lambda x:'Fixed rate day count (Interest Rates only) (repeatable up to 2 times semi-colon separated) must be in '+quote(dayCounts)),  #Fixed rate day count (Interest Rates only) (repeatable up to 2 times semi-colon separated)
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:number(x),                     lambda x:numberError),#Exchange rate 1 (Foreign exchange only)
	(lambda x:number(x),                     lambda x:numberError),#Forward exchange rate (Foreign exchange only)
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in commoditiesBase,      lambda x:'Commodity base (Commodities only - general) (repeatable up to 2 times semi-colon separated) must be one of '+quote(commoditiesBase)),#Commodity base (Commodities only - general) (repeatable up to 2 times semi-colon separated)
	(lambda x:x not in commodityDetails,     lambda x:'Commodity details (Commodities only - general) (repeatable up to 2 times semi-colon separated) must be one of '+quote(commodityDetails)),#Commodity details (Commodities only - general) (repeatable up to 2 times semi-colon separated)
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in energyCodes,          lambda x:'Load type (Commodities only - energy) must be one of '+quote(energyCodes)),#Load type (Commodities only - energy)                                                                    
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                       #Delivery start date and time (Commodities only - energy) (repeatable up to 12 times semi-colon separated)
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                       #Delivery end date and time (Commodities only - energy) (repeatable up to 12 times semi-colon separated)  
	(lambda x:False,                         lambda x:''),
	(lambda x:number(x),                     lambda x:numberError),                                                                        #Quantity Unit (Commodities only - energy) (repeatable up to 2 times semi-colon separated)                 
	(lambda x:number(x),                     lambda x:numberError),                                                                        #Price/time interval quantities (Commodities only - energy) (repeatable up to 24 times semi-colon separated)
	(lambda x:x not in bool_,                lambda x:'Clearing Threshold Party 1 must be in '+quote(bool_)),                     #Clearing Threshold Party 1                                                                                
	(lambda x:x not in bool_,                lambda x:'Clearing Threshold Party 2 must be in '+quote(bool_)),                     #Clearing Threshold Party 2                                                                                
	(lambda x:x not in bool_,                lambda x:'Compression must be in '+quote(bool_)),                                    #Compression                                                                                               
	(lambda x:v_datetime(x),                 lambda x:datetimeErr),                                                       #Confirmation Date Time                                                                                    
	(lambda x:x not in confirmationTypes,    lambda x:'Confirmation Type must be in '+quote(confirmationTypes)),                  #Confirmation Type                                                                                         
	(lambda x:x not in esma,                 lambda x:'Mandatory Clearing Indicator must be in '+quote(esma)),                    #Mandatory Clearing Indicator                                                                              
	(lambda x:False,                         lambda x:''),
	(lambda x:len(x)>256,                    lambda x:'Name of Trade Party 1 cant be longer than 256 chars'),                                         #Name of Trade Party 1                                
	(lambda x:len(x)>256,                    lambda x:'Name of Trade Party 2 cant be longer than 256 chars'),                                         #Name of Trade Party 2                                
	(lambda x:x not in assetIdTypes,         lambda x:'Underlying Asset Identifier Type (repeatable 2 times) must be one of '+quote(assetIdTypes)),   #Underlying Asset Identifier Type (repeatable 2 times)
	(lambda x:len(x)>100,                    lambda x:'Master Agreement type cant be more than 100 chars'),                                           #Master Agreement type                                
	(lambda x:len(x)>20,                     lambda x:'Master Agreement version cant be more than 20 chars'),                                         #Master Agreement version                             
	(lambda x:x not in tradeParties,         lambda x:'Leg /Currency 1 Payer should be one of:'+quote(tradeParties)),                                 #Leg /Currency 1 Payer                                
	(lambda x:x not in tradeParties,         lambda x:'Leg /Currency 2 Payer should be one of:'+quote(tradeParties)),                                 #Leg /Currency 2 Payer                                
	(lambda x:False,                         lambda x:''),
	(lambda x:False,                         lambda x:''),
	(lambda x:x not in"BS",                  lambda x:'buy/sell indicator should be B or S') #buy/sell indicator
	)
