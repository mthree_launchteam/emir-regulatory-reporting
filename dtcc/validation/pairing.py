import pickle
from dtcc.validation.util import table
def pairing(workDir,fileName):
	#do pairing
	#can't pair based on the filename because can potentially have different dates inside
	#need to keep a list of all the uti's so far, both for the fake side and their side.
	clientUTIFile=workDir+'/clientUTIs'
	fakeUTIFile=workDir+'/fakeUTIs'
	try:
		with open(clientUTIFile,'rb') as f:
			client=pickle.load(f)
	except:
		client=table()
	try:
		with open(fakeUTIFile,'rb') as f:
			fake=pickle.load(f)
	except:
		fake=table()

	#eg fileName==emir_report_2016.11.17.csv
	try:
		with open(workDir+'/fake_report_'+fileName.split('_')[2],'r') as f:
			fake.i(f)
	except:
		#TODO insert todays fake data if it's there..... MAKE sure it's there by generating it now!.... by calling another script.
		pass
	with open(fileName,'r')as currentFile:
		#read whole file into a structure
		#group by uti
		#do valuations
		#do positions
		#do transactions
		#try to match by ?????
		#identify whether each row pairs or not, then generate a report
		
		#student side
		#fake sidie
		duplicateRows=client.i([x for x in currentFile])
		if len(duplicateRows):
			pass
			#TODO generate some kind of report and don't bother with the rest of the pairing.
		#TODO mark the ones from this file when inserting, so that can select only the ones from this file, so that report doesn't report previous file errors, thats what the EOD report is supposed to do.
		utis=set(client.c('UTI Value'))
		for uti in utis:
			clientutigroup=client.s('UTI Value','==',uti)
			fakeutigroup=fake.s('UTI Value','==',uti)
			for row in clientutigroup:
				#TODO this will never be true as need to swap all the CCP1 stuff for CCP2, Side must be inverse too BS to SB
				if row in fakeutigroup:
					fakeutigroup.remove(row)
				else:
					pass
					#TODO find something else to match on, then if match then what part of the row doesn't, and if doesn't match(then print specific error) then not found error
					#eg product id, side, ....time with tolerance???
					#Amounts on trades to be compared must be within 1 percent difference.
					#Datetime on trades to be compared must match for the date part of the field.
					
					#ones that must match
					#No., Name, ESMA, Format, Match/, Tolerance Rule to other trade Category Details
					#1 Taxonomy used Identify the taxonomy used:
					#Must match 1
					
					#2 Product id 1 For taxonomy = U: Product Identifier (UPI), to be defined For taxonomy = I: ISIN or AII, 12 digits alphanumerical code For taxonomy = E: Derivative class: CO=Commodity CR=Credit CU=Currency EQ=Equity IR=Interest Rate OT= Other
					#Must match 1

					#3 Product id 2 For taxonomy = U: Blank For taxonomy = I: CFI, 6 characters alphabetical code For taxonomy = E: Derivative type: CD= Contracts for difference FR= Forward rate agreements FU= Futures FW=Forwards OP=Option SW=Swap OT= Other
					#Must match 1
											#
					#4 Underlying ISIN (12 alphanumerical digits); LEI (20 alphanumerical digits); Interim entity identifier (20 alphanumerical digits); UPI (to be defined); B= Basket; I=Index.
					#Must match 2
											#
					#5 Notional currency 1 ISO 4217 Currency Code, 3 alphabetical digits. 
					#Must match 1
					
					#6 Notional currency 2 ISO 4217 Currency Code, 3 alphabetical digits. 
					#Must match 1
											#
					#7 Deliverable ISO 4217 Currency Code, 3 alphabetical digits. 
					#May not match 3
											#
					#8 Trade id Up to 52 alphanumerical digits. 
					#Must match 1
											#
					#9 Transaction reference number
					#An alphanumeric field up to 40 characters Will never match 3
											#
					#10 Venue of execution ISO 10383 Market Identifier Code (MIC), 4 digits alphabetical. Where relevant, XOFF for listed derivatives that are traded off-exchange or XXXX for OTC derivatives.
					#Should match 2
											#
					#11 Compression Y = if the contract results from compression; N= if the contract does not result from compression.
					#Should match 2
											#
					#12 Price/rate Up to 20 numerical digits in the format xxxx,yyyyy.
					#Apply tolerance check 1 2
											#
					#13 Price notation E.g. ISO 4217 Currency Code, 3 alphabetical digits, percentage.
					#Must match 2
											#
					#14 Notional amount Up to 20 numerical digits in the format xxxx,yyyyy.
					#Apply tolerance check 2 1
											#
					#15 Price multiplier Up to 10 numerical digits.
					#Must match 1
											#
					#16 Quantity Up to 10 numerical digits.
					#Must match 1
											#
					#17 Up-front payment Up to 10 numerical digits in the format xxxx,yyyyy for payments made by the reporting counterparty and in the format xxxx,yyyyy for payments received by the reporting counterparty.
					#Apply tolerance check 1 2
											#
					#18 Delivery type C=Cash, P=Physical, O=Optional for counterparty.
					#Must match 2
											#
					#19 Execution timestamp ISO 8601 date format / UTC time format. 
					#Apply tolerance check 3 for OTC. 2
											#
					#20 Effective date ISO 8601 date format.
					#Must match 2
											#
					#21 Maturity date ISO 8601 date format.
					#Must match 1
											#
					#22 Termination date ISO 8601 date format.
					#Must match 2
											#
					#23 Date of settlement ISO 8601 date format.
					#Cannot match 3
											#
					#24 Master agreement type Free Text, field of up to 50 characters, identifying the name of the Master Agreement used, if any.
					#Cannot match 3
											#
					#25 Master agreement version Year, xxxx.
					#Cannot match 3
											#
					#26 Confirmation timestamp ISO 8601 date format, UTC time format. 
					#Apply tolerance check 3 2
											#
					#27 Confirmation means Y=Non-electronically confirmed, N=Nonconfirmed, E=Electronically confirmed.
					#Should match 2
											#
					#28 Clearing obligation Y=Yes, N=No.
					#Must match 2
											#
					#29 Cleared Y=Yes, N=No.
					#Must match 1
											#
					#30 Clearing timestamp ISO 8601 date format, UTC time format.
					#Apply tolerance check 3 2
											#
					#31 CCP Legal Entity Identifier (LEI) (20 alphanumerical digits) or, if not available, interim entity identifier (20 alphanumerical digits) or, if not available, BIC (11 alphanumerical digits).
					#Must match 2
											#
					#32 Intragroup Y=Yes, N=No. 
					#Should match 2
											#
					#33 Fixed rate leg 1 (tolerance tba) Numerical digits in the format xxxx,yyyyy. 
					#Apply tolerance check 1 2
											#
					#34 Fixed rate leg 2 (tolerance tba) Numerical digits in the format xxxx,yyyyy. 
					#Apply tolerance check 1 2
											#
					#35 Fixed rate day count Actual/365, 30B/360 or Other.
					#Cannot match 3
											#
					#36 Fixed leg payment frequency An integer multiplier of a time period describing how often the counterparties exchange payments, e.g. 10D, 3M, 5Y.
					#Cannot match 3
											#
					#37 Floating leg payment frequency An integer multiplier of a time period describing how often the counterparties exchange payments, e.g. 10D, 3M, 5Y.
					#Cannot match 3
											#
					#38 Floating leg reset frequency An integer multiplier of a time period describing how often the counterparties exchange payments, e.g. 10D, 3M, 5Y.
					#Cannot match 3
											#
					#39 Floating rate of leg 1 The name of the floating rate index, e.g. 3M Euribor.
					#Cannot match 3
											#
					#40 Floating rate of leg 2 The name of the floating rate index, e.g. 3M Euribor.
					#Cannot match 3
											#
					#41 Currency 2 ISO 4217 Currency Code, 3 alphabetical digits.
					#Must match 2
											#
					#42 Exchange rate 1 Up to 10 numerical digits in the format xxxx,yyyyy.
					#Must match Tolerance check 1 will be applied 2
											#
					#43 Forward exchange rate Up to 10 numerical digits in the format xxxx,yyyyy.
					#Must match Tolerance check 1 will be applied 2
											#
					#44 Exchange rate basis E.g. EUR/USD or USD/EUR. 2 X ISO 4217 Currency Code, 3 alphabetical digits separated by a slash.
					#Must match 2
											#
					#45 Commodity Base AG=Agricultural EN=Energy FR=Freights ME=Metals IN= Index EV= Environmental EX= Exotic
					#Should match 2
											#
					#46 Commodity AG=Agricultural Should match 2 
					#EN=Energy FR=Freights ME=Metals IN= Index EV= Environmental EX= Exotic
											#
					#47 Delivery Point or zone (energy)
					#EIC code, 16 character alphanumeric code. Must match 2
											#
					#48 Interconnection point (energy)
					#Free text, field of up to 50 characters. Cannot match 3
											#
					#49 Load Type (energy) Repeatable section of fields 50-54 to identify the product delivery profile; BL=Base Load PL=Peak Load OP=Off-Peak BH= Block Hours OT=Other
					#Must match 2
											#
					#50 Delivery start date time(energy)
					#ISO 8601 date format. Must match 2
											#
					#51 Delivery end date time (energy)
					#ISO 8601 date format. Must match 2
											#
					#52 Contract Capacity(energy)
					#Free text, field of up to 50 characters. Cannot match 3
											#
					#53 Quantity Unit (energy) 10 numerical digits in the format xxxx,yyyyy.
					#Apply tolerance check 1 1
											#
					#54 Price/time interval quantities (energy) 10 numerical digits in the format xxxx,yyyyy.
					#Apply tolerance check 1 2
											#
					#55 Option type P=Put, C=Call.
					#Must match 2
											#
					#56 Option style A=American, B=Bermudan, E=European, S=Asian.
					#Must match 2
											#
					#57 Strike price Up to 10 Numerical digits in the format xxxx,yyyyy.
					#Apply tolerance check 2 2
					#################################################

					#ones that can differ with a ??warning?? but not an error
					
					#TODO ....error....
			if len(fakeutigroup):
				pass
				#TODO error.....
	#TODO transaction vs position vs valuation
	#TODO do the transactions properly aggregate into positions and valuations
