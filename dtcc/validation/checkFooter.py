from dtcc.validation.util import readline, err
import re
def checkFooter(bodyLength, currentFile, l):
	#We don't process Constant1 because it never changes and the line was read before calling this function
	####Footer
	#####Constant1,DTC0-END00054321
	#####Originator,M3__
	#####Constant2,-END
	#####Record_Count=00000001
	l, line = readline(l,currentFile)
	if not re.match(r'Originator,.*',line):
		err('expected\nOriginator',l)
	if len(line.split(',')[1])!=4:
		err('Originator must be 4 characters',l)

	l, line = readline(l,currentFile)
	if line!='Constant2,-END':
		err('Expected\nConstant2,-END',l)

	l, line = readline(l,currentFile)
	if line.split('=')[0]!='Record_Count':
		err('expected\nRecord_Count',l)
	if line.split('=')[1]!=str(bodyLength).zfill(8):
		err('expected\nRecord_Count='+str(bodyLength).zfill(8),l)
