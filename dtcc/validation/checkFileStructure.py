from dtcc.validation.util import readline, err
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames


def checkFileStructure(currentFile_):
    #global currentFile
    #currentFile=currentFile_
    l=0
    numExpectedCols=128+28+1 #128 ESMA, 28GTR, 1Missing from spec-Buy/Sell
    l,line=readline(l, currentFile_) #should be a list of column numbers '1,2,3,....'
    colNums=','.join([str(x) for x in range(1,numExpectedCols+1)]) #add 2: 1 for 0->1 shift, and 1 for buy/sell indicator
    if line!=colNums:
        err('Expected column numbers\n'+colNums,l)
    l,line=readline(l,currentFile_) #should be a list of column names 'Comment,Version,Message Type,...'
    columns=','.join(DTCC_ColumnNames)
    if line!=columns:
        # Don't be overly helpful in the error message that goes back to the client. However we can put helpful info in for debugging using 'print'
        print('Column name mismatch')
        theirCols=line.split(',')
        tlen=len(theirCols)
        olen=len(DTCC_ColumnNames)
        print("their count {} our count {}".format(tlen,olen))
        if tlen==olen:
            for theirs,ours in zip(theirCols,DTCC_ColumnNames):
                prefix="same "if theirs==ours else "diff "
                print("{} their col '{}' our col '{}'".format(prefix,theirs,ours))
        err('Expected column names\n'+','.join(DTCC_ColumnNames),l)
    l,line=readline(l,currentFile_) #*Comment line
    if len(line)==0 or line[0]!='*':
        err('Line '+str(l)+' must be a comment line starting with "*"',l)
    return l,numExpectedCols
