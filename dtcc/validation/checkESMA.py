from dtcc.validation.util import err, get
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames
from dtcc.columns.naCollateralFields import naCollateralFields
from dtcc.columns.naValuationFields import naValuationFields
from dtcc import cols
import dtcc
from dtcc.columns.naPositionFields import naPositionFields

def checkNA_Fields(naFieldList,listTypeStr):
	for i in range(0,len(naFieldList)):
		#get the next field that must be blank, get it's index in the main list and then look up the value in this row->needs to be ''
		if cols[DTCC_ColumnNames.index(naFieldList[i])]!='':
			err('Expected column '+naFieldList[i]+' to be empty as not applicable to '+dtcc.MessageType+' messages')
	
#checks all the ESMA business logic rules
def checkESMA(line):
	cols=line.split(',')
	##gtr core validation
	#do the gtr fields have the correct values and combine to make sense
	if cols[dtcc.MessageType]=="Position" and cols[dtcc.Action]=='Cancel':
		err('Cannot cancel a position message',l)
	
	if cols[dtcc.MessageType]=='Collateral':
		checkNA_Fields(naCollateralFields)
	elif cols[dtcc.MessageType]=='Valuation':
		checkNA_Fields(naValuationFields)
	elif cols[dtcc.MessageType]=='Position':
		checkNA_Fields(naPositionFields)
	
	if get('Additional Repository 1 Value')!='' and get('Additional Repository 1 Prefix')=='':
		err('Need Additional Repository 1 Prefix whe providing Additional Repository 1 Value',l)
	##business validation
	#Validate(report.csv)->warnings -> make sure to reject any 'new' UTI's that have already been accepted 
	#are the emir fields consistent within a row and across rows
	#are all the required fields populated
	if cols[dtcc.MessageType]=='Position':
		if get('Reporting Obligation Party 1')!='' or get('Trade Party 1 Non-financial Entity Jurisdiction')=='ESMA':
			errMsg1='If "Reporting Obligation Party 1" is blank then Optional, otherwise If "Trade Party 1 Non-financial Jurisdiction" is populated with "ESMA" then Optional, otherwise Required'
			if get('Trade Party 1 Corporate Sector')=='':
				err(errMsg1,l)
			if get('Trade Party 1 Financial Entity Jurisdiction')=='':
				err(errMsg1)
		if (get('Reporting Obligation Party 2')!='' or get('Trade Party 2 Non-financial Entity Jurisdiction')=='ESMA'):
			errMsg2='If "Reporting Obligation Party 2" is blank then Optional, otherwise If "Trade Party 2 Non-financial Jurisdiction" is populated with "ESMA" then Optional, otherwise Required'
			if get('Trade Party 2 Corporate Sector')=='':
				err(errMsg2)
		if get('Broker Id Party 1 Value')!='' and get('Broker Id Party 1 Prefix')=='':
			err('Broker Id Party 1 Prefix must be populated when Broker Id Party 1 Value is populated')
		if get('Broker Id Party 2 Value')!='' and get('Broker Id Party 2 Prefix')=='':
			err('Broker Id Party 2 Prefix must be populated when Broker Id Party 2 Value is populated')
		if get('Clearing Broker Party 1 Value')!='' and get('Clearing Broker Party 1 Prefix')=='':
			err('if "Clearing Broker Party 1 Value" is populated then Required, otherwise Optional')
		if get('Clearing Broker Party 2 Value')!='' and get('Clearing Broker Party 2 Prefix')=='':
			err('if "Clearing Broker Party 2 Value" is populated then Required, otherwise Optional')
		if get('Beneficiary ID Party 1 Value')!='' and get('Beneficiary ID Party 1 Prefix')=='':
			err('must have a Beneficiary ID Party 1 Prefix when giving a Beneficiary ID Party 1 Value')
		if get('Beneficiary ID Party 2 Value')!='' and get('Beneficiary ID Party 2 Prefix')=='':
			err('must have a Beneficiary ID Party 2 Prefix when giving a Beneficiary ID Party 2 Value')
		if get('Trading capacity Party 1')=='Agent' and get('Beneficiary ID Party 1 Value')=='':
			err('Beneficiary ID Party 1 Value must be populated when Trading capacity Party 1 is Agent')
		if get('Buyer Value (Party 1)')!='' and get('Buyer Prefix (Party 1)')=='':
			err('Need Buyer Prefix (Party 1) when giving Buyer Value (Party 1)')
		if get('Buyer Value (Party 2)')!='' and get('Buyer Prefix (Party 2)')=='':
			err('Need Buyer Prefix (Party 2) when giving Buyer Value (Party 2)')
		if get('Leg 1 Payer')=='' and get('Leg 2 Payer')=='' and get('Buyer Value (Party 1)')=='':
			err('Buyer Value (Party 1) must be populated if Leg 1 Payer and Leg 2 Payer arent')
		if get('Execution Venue Value')not in['XXXX','XOFF'] and get('Execution Venue Prefix')=='':
			err('Execution Venue Prefix is required when Execution Venue Value is not in XXXX,XOFF')
		if get('Upfront payment (repeatable up to 6 times semi-colon separated)')!='' and get('Upfront payment CCY (repeatable up to 6 times semi-colon separated)')=='':
			err('Upfront payment CCY (repeatable up to 6 times semi-colon separated) must be given when giving Upfront payment (repeatable up to 6 times semi-colon separated)')
		if get('Clearing DCO Value')!=''and get('Clearing DCO Prefix')=='':
			err('Clearing DCO Prefix is required whe giving Clearing DCO Value')
		if get('Option Strike Price')!=''and get('Option Strike Price Type')==''and get('Option Strike Price CCY')=='':
			err('If "Option Strike Price" is populated and "Option Strike Price Type" is blank then Required, otherwise Optional')
		if  get('Underlying Asset (repeatable up to 2 times semi-colon separated)')!='' and get('Underlying Asset Identifier Type (repeatable 2 times)')=='':
			err('if "Underlying Asset" is populated then Required, otherwise Optional')
		if get('Option Strike Price')!=''and get('Option Strike Price Type')=='':
			err('If "Option Strike Price" is populated and "Option Strike Price CCY" is blank then Required, otherwise Optional')
	#applicable to all
	
	if get('Submitted For Value')not in ['','BOTH'] and get('Submitted For Prefix')not in dtcc.companyIdTypes:
		err('When Submitted For Value is not BOTH or blank then Submitted For Prefix must be in '+dtcc.companyIdTypesString)
	if get('Submitted For Value')=='BOTH' and get('Reporting Delegation Model')!='Full':
		err('When Submitted For Value is BOTh then Reporting Delegation Model should be Full')
	elif get('Submitted For Value')!='BOTH' and get('Reporting Delegation Model')!='Independent':
		err('When Submitted For Value is NOT BOTh then Reporting Delegation Model should NOT be Full')
	if get('Execution Agent Party 1 Value')!='' and get('Execution Agent Party 1 Prefix')=='':
		err('must have Execution Agent Party 1 Prefix when giving Execution Agent Party 1 Value')
	if get('Execution Agent Party 2 Value')!='' and get('Execution Agent Party 2 Prefix')=='':
		err('must have Execution Agent Party 2 Prefix when giving Execution Agent Party 2 Value')
	if get('Trade Party 1 Prefix')in['SWIFTBIC','EIC','INTERNAL','FREEFORMATTEXT']and get('Name of Trade Party 1')=='':
		err('if "Trade Party 1 Prefix" is "SWIFTBIC" or "EIC" or "INTERNAL" or "FREEFORMATTEXT" then Required, otherwise Optional')
	if get('Trade Party 2 Prefix')in['SWIFTBIC','EIC','INTERNAL','FREEFORMATTEXT']and get('Name of Trade Party 2')=='':
		err('if "Trade Party 2 Prefix" is "SWIFTBIC" or "EIC" or "INTERNAL" or "FREEFORMATTEXT" then Required, otherwise Optional')
	if cols[dtcc.MessageType]in['Position','Valuation']:
		if get('MTM Value Party 1')!=''and get('MTM Currency Party 1')=='':
			err('must specify the MTM Currency Party 1 when giving MTM Value Party 1')
		if get('MTM Value Party 1')!=''and get('Valuation Datetime Party 1')=='':
			err('must give Valuation Datetime Party 1 when giving MTM Value Party 1')
		if get('MTM Value Party 1')!=''and get('Valuation Type Party 1')=='':
			err('must give Valuation Type Party 1 when giving MTM Value Party 1')
