import pytest
import create_trades2
import pandas as pd
import insert_trades
import sqlalchemy
from insert_trades import *
from create_trades2 import * 


def test_does_generatTradeData_return_dictionary():
        a=generateTradeData()
        b=generateTradeData()
        assert type(a)==dict
        assert len(a)==8
        assert a!=b

def test_does_insertTrades_append_table_in_database():
        a=generateTradeData()
        make_table("some_table","mysql+pymysql://root:@localhost:3306/project")
        insertTrades("mysql+pymysql://root:@localhost:3306/project","some_table",a)
        some_table=pd.read_sql_table("some_table","mysql+pymysql://root:@localhost:3306/project")
        assert len(some_table["instrument"])>0