import pytest
import create_trades2
import pandas as pd
import insert_trades
import sqlalchemy
import generateDB
import migrateDB

from migrateDB import *
from generateDB import *
from insert_trades import *
from create_trades2 import * 

def test_does_migrateDB_copy_table_to_another_DB():
        migrateDB("mysql+pymysql://root:@localhost:3306/project","mysql+pymysql://root:@localhost:3306/test")
        c=pd.read_sql_table("some_table","mysql+pymysql://root:@localhost:3306/project")
        d=pd.read_sql_table("some_table","mysql+pymysql://root:@localhost:3306/test")
        assert c["instrument"][1]==d["instrument"][1]
