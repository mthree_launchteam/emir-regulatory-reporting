import re
import dtcc
import pytest 
from dtcc.columns.DTCC_ColumnNames import DTCC_ColumnNames
from dtcc import cols

###############################
#Error handling
###############################
#dtcc.py reads in emir reports and validates them, it has to look at each record/field, if the data is invalid then this function generates the error
#it relies on a currentCol global variable which keeps track of which column we are processing
#fundamental format errors causes processing of the file to stop
#TODO consider making currentCol an argument 
currentCol=None
def err(text,index=None): #TODO what is index for?
	colNum='' if currentCol is None else '. Column '+currentCol+' . '
	raise ValueError('error: line:'+str(l)+' col:'+colNum+'. '+text)
#if the file is in the correct format, but is logically inconsistent, then we compile a list of rejection reasons
def rej(text):
	#TODO we are putting duplicates in the nack, is that intentional? surely not?
	dtcc.nack.append(str(dtcc.line)+',reject') #append this line to list of rejected lines
	dtcc.warn.append("msgId="+dtcc.msgIds[-1]+" "+text)
###############################
#File processing
###############################
#Python3 makes reading a line ugly, so we wrap it here to make the code more readable
def readline():
	global l #l is the current line number
	l+=1
	return dtcc.currentFile.readline().decode("utf-8")[:-1] #we don't want the \n at the end
####################
#Given the name of the column, find it's index and return the value from the current record 
def get(name):
	return cols(DTCC_ColumnNames.index(name))


###############################
#Field validation
###############################
#validate datetime format of the current field
#TODO rename to v_datetime
#False is good, True is bad
def v_datetime(index):
	if cols(index)is'': #if value we are validating is blank, that is a valid date
		return False
	return not re.match(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$',cols(index))
#validate float number format, maybe allow nulls
def number(index,canBeNull=True):
	if canBeNull and (cols(index) is '' or cols(index) == 0): #null date can be missing or 0
		return False
	if not re.match(r'.*[a-zA-Z].*^$' ,cols(index)):
		print('numbers must not contain letters')
		return True
	whole,fractional=cols(index).split('.')
	if len(whole)>25:
		print('numbers must be at most 25 whole digits and 10 fractional')
		return True
	if len(fractional)>10:
		print('numbers must be at most 25 whole digits and 10 fractional')
		return True
	try: #the casts will error if the string isn't a number so catch and reject
		if whole[0]=='-':
			int(whole[1:])
		else:
			int(whole)
		int(fractional)
		return False
	except:
		print('not a number')
		return True
#validate int number format, maybe allow nulls
def wholeNumber(index):
	try:
		int(cols(index))
		return False
	except:
		return True
#TODO rename to v_date
#validate date
def v_date(index):
	if cols(index)is'':
		return False
	return not re.match(r'^[0-9]{4}-[0-9]{2}-[0-9]{2}',cols(index))

###############################
#Formatting
###############################
#format list as quoted csv
#TODO is there an inbuilt for this in python3?
def quote(list_):
	return  "'"+str("','".join(list_))+"'"

#############
#TODO replace with pandas	
class table:
	def __init__(self,square=[]):
		self.data=[self.__parse(line) for line in square]

	def __parse(self, line):
		return {key:value for key,value in zip(DTCC_ColumnNames,line.split(','))}

	def has_ready_log(self):
		if self.__parse is None:
			return False
		else:
			return True
	
	def c(self,colName): #column
		col=[row[colName]for row in self.data]
		return col

	def s(self,col,op,value): #select
		for row in self.data:
			if op=='>':
				if float(row[col])>value:
					return [row]
			if op=='<':
				if float(row[col])<value:
					return [row]
			if op=='=':
				if float(row[col])==value:
					return [row]


	def i(self,newData): #insert
		duplicateRows=[]
		#TODO make this more sophisticated by processing cancels/modifies
		for line in newData:
			newRow=self.__parse(line)
			if newRow in self.data:
				duplicateRows.append(int(newRow['Data Submitter Message ID']))  #zmiana z += na append bo inaczeje dodajemy listę do inta 
			else:
				self.data.append(newRow)
		return duplicateRows

