import pytest
import create_trades2
import pandas as pd
from create_trades2 import * #wtedy wczytuje też globalne zmienne w funkcjach 




def test_does_fun_connnect_work():
        assert connect("mysql+pymysql://root:@localhost:3306/project")!=None


def test_wrong_path_raise_an_exception():
        with pytest.raises(Exception):
                assert connect("sdf")
        
def test_was_table_created():
        engine,con=connect("mysql+pymysql://root:@localhost:3306/project")
        some_table=pd.read_sql_table("some_table",engine)
        assert some_table.all!=None   #sprawdza wszystkie wartości z tabeli

def test_using_wrong_argument_in_make_table_raise_an_exception():
        with pytest.raises(Exception):
                assert make_table(sdf,"mysql+pymysql://root:@localhost:3306/project")

def test_creating_table_that_already_exist_dont_raise_an_exception(): #no assert needed, if there will be no error or exception test will pass
        connect("mysql+pymysql://root:@localhost:3306/project")
        make_table("some_table","mysql+pymysql://root:@localhost:3306/project")


# @pytest.fixture
# def connect():
#     from create_trades2 import connect
#     return connect
