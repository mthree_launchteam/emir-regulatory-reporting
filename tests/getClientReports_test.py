import pytest
from ftplib import FTP
import os
from ftplib import FTP
import datetime

import sys
sys.path.insert(0,'C:\\Users\\Patryk Surma\\Desktop\\cc')

from checkFileStructure import *
from parseLine import *
from checkFooter import *
from checkESMA import *
from pairing import pairing
from util import readline, err
from getClientReports import getClientReports
from report import ackNackReport, warningReport, fileProcessingReport,	ESMA_Match_Status_Report


from innit import ftpIP,ftpDir,filesProcessedFilename,workDir


def test_does_getClientReports_return_list_of_paths():
        x=getClientReports(ftpIP,ftpDir,filesProcessedFilename) 
        assert  type(x)==list
        assert  type(x[0])==str

def test_does_getClientReports_raise_error_when_using_wrong_ftp_etc():
        with pytest.raises(Exception):
                assert getClientReports("aaa",ftpDir,filesProcessedFilename)
                assert getClientReports(ftpIP,"bbb",filesProcessedFilename)
                assert getClientReports(ftpIP,ftpDir,"ccc")
