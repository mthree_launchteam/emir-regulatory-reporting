import calc
import util
import pytest 
from util import table
"""
Created on Wed Jul 17 13:47:46 2019

@author: wiecz
"""
##inicial data#
table1=table(["5,6,7"])   #jeżeli tutaj będą znaki a nie liczby to funkcja s nie ma sensu. będzie się wtedy porównywać string z intem 
newData=["1,2,3,4,5,6,7,8,9"] #musi być ich więcej niż Data Submitter Message ID (8) bo inaczej funkcja i nie ma sensu

###parse####
def test_does_parse_exist():
    par = table([])
    assert par.has_ready_log() == True

###DTCC_ColumnNames###
def	test_is_DTCC_ColumnNames_a_tuple():
	assert type(util.DTCC_ColumnNames)==tuple

def test_is_DTCC_ColumnNames_not_empty():
	assert len(util.DTCC_ColumnNames)!=0

###data###
def test_self_data_exist():
	print(table1.data)
	assert type(table1.data)==list

###c###
def test_is_col_a_list():
	col=table1.c('Version')
	assert type(col)==list

###s###
def test_s_is_a_method():
	ss=table1.s('Version','>',2)
	assert type(ss)==list

###i###
def test_is_duplicateRows_not_empty():
	table1.i(newData)
	assert len(table1.i(newData))>0


def test_is_parse_a_dict():
	par=table([])
	assert par._table__parse(line)==dict



# ------------------------------------ resources
#@pytest.fixture
def table_class():
    from util import table
    return table





# #@pytest.mark.number
# def test_add():
# 	assert calc.add(10,2) == 12 
# 	assert calc.add(10,10) == 20
# 	assert calc.add(10) == 12

# #@pytest.mark.number
# def test_product():
# 	assert calc.sub(10,2) == 8
# 	assert calc.sub(5,5) == 0	
	
	
# #@pytest.mark.strings
# def test_add_string():
# 	result = calc.add('Hello','Python')
# 	assert result == 'HelloPython'
# 	assert type(result) is str 
# 	assert 'Hello' in result


# '''

# @pytest.mark.X expect a parameter during the execution

# Eg: 
	
# 	pytest -m number test_calc.py ## Runs the test case methods which are marked as number
	
	

# '''

